var fs = require('fs');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'aBc1S4df';

var filename = process.argv[2]
fs.readFile(filename, function (err,data) {
    if (err) {
        return console.log(err);
    }
    var cipher = crypto.createCipher(algorithm, password);
    var text = data;
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    var output = filename.replace('.pdf','').concat("_Encrypted.txt");
    fs.writeFileSync(output, crypted);
});  
