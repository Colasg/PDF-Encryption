var fs = require('fs');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'aBc1S4df';

    var filename = process.argv[2];
    var text = fs.readFileSync(filename, 'utf8');
    var decipher = crypto.createDecipher(algorithm, password);
    decipher.setAutoPadding(false);
    var dec = decipher.update(text,'hex','binary');                               
    dec += decipher.final('binary');                                           
    var buffer = new Buffer(dec, "binary");  
    var output = filename.replace('_Encrypted.txt','_Decrypted.pdf')                                    
    fs.writeFileSync(output, buffer, 'binary');  
// });
       


